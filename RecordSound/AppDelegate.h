//
//  AppDelegate.h
//  RecordSound
//
//  Created by Guillermo Moral on 10/2/14.
//  Copyright (c) 2014 GM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
