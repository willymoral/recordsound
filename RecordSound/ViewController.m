//
//  ViewController.m
//  RecordSound
//
//  Created by Guillermo Moral on 10/2/14.
//  Copyright (c) 2014 GM. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(nonatomic, strong)NSMutableDictionary * recordSettings;
@property(nonatomic, strong)NSURL *soundFileURL;
@property(nonatomic, strong)NSString *typeOfFile;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.playButton.enabled = NO;
    self.stopButton.enabled = NO;
    
    self.recordSettings = [[NSMutableDictionary alloc] init];
    self.sampleRate.selectedSegmentIndex = 3;
    self.encoding.selectedSegmentIndex = 0;
    self.quality.selectedSegmentIndex = 0;
    self.chanel.selectedSegmentIndex = 0;
}

-(void)createFile {
    NSArray *dirPaths;
    NSString *docsDir;
    
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    switch (self.quality.selectedSegmentIndex) {
        case 0:
            self.typeOfFile =@"-Min";
            break;
            
        case 1:
            self.typeOfFile =@"-Low";
            break;
            
        case 2:
            self.typeOfFile =@"-Med";
            break;
            
        case 3:
            self.typeOfFile =@"-Hig";
            break;
            
        case 4:
            self.typeOfFile =@"-Max";
            break;
    }

    switch (self.encoding.selectedSegmentIndex) {
        case 0:
            self.typeOfFile = [NSString stringWithFormat:@"%@-%@",self.typeOfFile,@"MPEG.m4a"];
            break;
        case 1:
            self.typeOfFile = [NSString stringWithFormat:@"%@-%@",self.typeOfFile,@"LBC.caf"];
            break;
        case 2:
            self.typeOfFile = [NSString stringWithFormat:@"%@-%@",self.typeOfFile,@"AAC.aac"];
            break;
        case 3:
            self.typeOfFile = [NSString stringWithFormat:@"%@-%@",self.typeOfFile,@"IMA4.caf"];
            break;
    }
    
    NSString *soundFilePath = [docsDir
                               stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",[self soundHash], self.typeOfFile]];
        
    self.soundFileURL = [NSURL fileURLWithPath:soundFilePath];
}

-(void)settingRecord:(BOOL)record {
    
    
    NSError *error = nil;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    if(record) {
        [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];
         
         self.audioRecorder = [[AVAudioRecorder alloc]
                               initWithURL:self.soundFileURL
                               settings:self.recordSettings
                               error:&error];
         
         if (error)
         {
             NSLog(@"error: %@", [error localizedDescription]);
         } else {
             [self.audioRecorder prepareToRecord];
         }

    } else {
        [audioSession setCategory:AVAudioSessionCategoryPlayback
                            error:nil];
    }

    self.chronometer.text = @"00:00:00";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)settingSample {
   
    [self.recordSettings removeAllObjects];
    
    switch (self.encoding.selectedSegmentIndex) {
        case 0:
            [self.recordSettings setValue :[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
            break;
        case 1:
            [self.recordSettings setValue :[NSNumber numberWithInt:kAudioFormatiLBC] forKey:AVFormatIDKey];
            break;
        case 2:
            [self.recordSettings setValue :[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
            break;
        case 3:
            [self.recordSettings setValue :[NSNumber numberWithInt:kAudioFormatAppleIMA4] forKey:AVFormatIDKey];
            break;
    }
    
    switch (self.sampleRate.selectedSegmentIndex) {
        case 0:
            [self.recordSettings setValue :[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
            break;
        case 1:
            [self.recordSettings setValue :[NSNumber numberWithFloat:32000.0] forKey:AVSampleRateKey];
            break;
        case 2:
            [self.recordSettings setValue :[NSNumber numberWithFloat:16000.0] forKey:AVSampleRateKey];
            break;
        case 3:
            [self.recordSettings setValue :[NSNumber numberWithFloat:8000.0] forKey:AVSampleRateKey];
            break;
    }
    
    switch (self.quality.selectedSegmentIndex) {
        case 0:
            [self.recordSettings setValue :AVAudioQualityMin forKey:AVEncoderAudioQualityKey];
            break;
            
        case 1:
            [self.recordSettings setValue :[NSNumber numberWithInt:AVAudioQualityLow] forKey:AVEncoderAudioQualityKey];
            break;
            
        case 2:
            [self.recordSettings setValue :[NSNumber numberWithInt:AVAudioQualityMedium] forKey:AVEncoderAudioQualityKey];
            break;
            
        case 3:
            [self.recordSettings setValue :[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
            break;
            
        case 4:
            [self.recordSettings setValue :[NSNumber numberWithInt:AVAudioQualityMax] forKey:AVEncoderAudioQualityKey];
            break;
    }

    switch (self.chanel.selectedSegmentIndex) {
        case 0:
            [self.recordSettings setValue :[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
            break;
        case 1:
            [self.recordSettings setValue :[NSNumber numberWithInt:2] forKey:AVNumberOfChannelsKey];
            break;
    }
    
    NSLog(@"%@",self.recordSettings.description);
}

- (IBAction)encodignChange:(id)sender {
    switch (self.encoding.selectedSegmentIndex) {
        case 0:
            self.sampleRate.enabled = YES;
            break;
        case 1:
            self.sampleRate.selectedSegmentIndex = 3;
            self.sampleRate.enabled = NO;
            break;
        case 2:
            self.sampleRate.enabled = YES;
            break;
        case 3:
            self.sampleRate.enabled = YES;
            break;
    }
}

- (IBAction)record:(id)sender {
    [self createFile];
    [self settingSample];
    [self settingRecord:YES];
    
    if (!_audioRecorder.recording)
    {
        _playButton.enabled = NO;
        _stopButton.enabled = YES;
        [_audioRecorder record];
        self.starDate = [NSDate date];
        self.timerTick = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
    }
}

- (IBAction)play:(id)sender {
    [self settingRecord:NO];
    if (!_audioRecorder.recording)
    {
        _stopButton.enabled = YES;
        _recordButton.enabled = NO;
        
        NSError *error;
        
        _audioPlayer = [[AVAudioPlayer alloc]
                        initWithContentsOfURL:_audioRecorder.url
                        error:&error];
        
        _audioPlayer.delegate = self;
        
        if (error)
            NSLog(@"Error: %@",
                  [error localizedDescription]);
        else
            self.chronometer.text = @"00:00:00";
            self.starDate = [NSDate date];
            [_audioPlayer play];
        self.timerTick = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(refreshTimer) userInfo:nil repeats:YES];
    }
}

- (IBAction)stop:(id)sender {
    [self stop];
}

-(void)stop {
    self.stopButton.enabled = NO;
    self.playButton.enabled = YES;
    self.recordButton.enabled = YES;
    
    if (self.audioRecorder.recording)
    {
        [self.audioRecorder stop];
        self.endDate = [NSDate date];
    } else if (_audioPlayer.playing) {
        [self.audioPlayer stop];
    }
    
    [self.timerTick invalidate];
}

#pragma mark - Private Method

-(void)refreshTimer {
    self.endDate = [NSDate date];
    
    float secs = [self.endDate timeIntervalSinceDate:self.starDate];
	
	int tempHour    = secs / 3600;
    int tempMinute  = secs / 60 - tempHour * 60;
    int tempSecond  = secs - (tempHour * 3600 + tempMinute * 60);
    
    NSString *tmpHour = [NSString stringWithFormat:@"%i",tempHour];
    NSString *tmpMin = [NSString stringWithFormat:@"%i",tempMinute];
    NSString *tmpSec = [NSString stringWithFormat:@"%i",tempSecond];
    
    if([tmpHour length]==1){
        tmpHour = [NSString stringWithFormat:@"0%@",tmpHour];
    }
    
    if([tmpMin length]==1){
        tmpMin = [NSString stringWithFormat:@"0%@",tmpMin];
    }
    
    if([tmpSec length]==1){
        tmpSec = [NSString stringWithFormat:@"0%@",tmpSec];
    }
    
    self.chronometer.text = [NSString stringWithFormat:@"%@:%@:%@",tmpHour, tmpMin ,tmpSec];
}

- (NSString*)soundHash {
    NSString *ret;
    NSDate *currentDate = [NSDate date];
    NSTimeInterval interval = [currentDate timeIntervalSince1970];
    ret = [NSString stringWithFormat:@"%08x",(int)interval];
    return ret;
}

#pragma mark - AudioPlayerDelegate

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    _recordButton.enabled = YES;
    _stopButton.enabled = NO;
    [self.timerTick invalidate];
}

-(void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    NSLog(@"Decode Error occurred");
}

-(void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{

}

-(void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
    NSLog(@"Encode Error occurred");
}

@end
