//
//  ViewController.h
//  RecordSound
//
//  Created by Guillermo Moral on 10/2/14.
//  Copyright (c) 2014 GM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>

@property (strong, nonatomic) AVAudioRecorder *audioRecorder;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;

@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UILabel *chronometer;

@property (weak, nonatomic) IBOutlet UISegmentedControl *encoding;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sampleRate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *quality;
@property (weak, nonatomic) IBOutlet UISegmentedControl *chanel;

@property (strong, nonatomic) NSDate *starDate;
@property (strong, nonatomic) NSDate *endDate;
@property (strong, nonatomic) NSTimer *timerTick;
@property (strong, nonatomic) NSString *currentFile;

- (IBAction)encodignChange:(id)sender;
- (IBAction)record:(id)sender;
- (IBAction)play:(id)sender;
- (IBAction)stop:(id)sender;

@end
